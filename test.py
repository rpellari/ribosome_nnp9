import IMP
import IMP.pmi
import IMP.pmi.io
import IMP.pmi.io.crosslink
import IMP.pmi.restraints
import IMP.pmi.restraints.crosslinking
from IMP.pmi.io.crosslink import FilterOperator as FO
#import IMP.crosslinkms
#import IMP.crosslinkms.restraint_pmi2
import operator

rplp=IMP.pmi.io.crosslink.ResiduePairListParser("MSSTUDIO")
cldbkc=IMP.pmi.io.crosslink.CrossLinkDataBaseKeywordsConverter(rplp)
cldbkc.set_protein1_key("Protein 1")
cldbkc.set_protein2_key("Protein 2")
cldbkc.set_site_pairs_key("Selected Sites")
cldbkc.set_id_score_key("Score")
cldb=IMP.pmi.io.crosslink.CrossLinkDataBase(cldbkc)
cldb.set_name("master")

for csv in ["Resultat.b.csv"]:

    cldb_tmp=IMP.pmi.io.crosslink.CrossLinkDataBase(cldbkc)

    cldb_tmp.create_set_from_file(csv)

    #cldb_tmp.rename_proteins({"TssKHis6":"TssK","StrepTssF":"TssF","TssGFlag":"TssG","HATssE":"TssE"})
    cldb_tmp.set_name(csv)
    cldb.append_database(cldb_tmp)

cldb.align_sequence('tools/pairwise_alignment_NeisseriaTo_3ja1n_0.08.txt')

renamedict={}
for xl in cldb:
     try:
         protname=xl[cldb.protein1_key].split("|")[2]
         renamedict[xl[cldb.protein1_key]]=protname
     except:
         pass
     try:
         protname=xl[cldb.protein2_key].split("|")[2]
         renamedict[xl[cldb.protein2_key]]=protname
     except:
         pass

cldb.rename_proteins(renamedict)


#print(cldb)
