import IMP
import RMF
import IMP.atom
import IMP.rmf
import IMP.pmi
import IMP.pmi.topology
import IMP.pmi.dof
import IMP.pmi.macros
import IMP.pmi.restraints
import IMP.pmi.restraints.stereochemistry
import IMP.pmi.restraints.em
import IMP.pmi.plotting
import IMP.pmi.plotting.topology
import tempfile,os
import sys
import IMP.em
import numpy as np
import math


output_objects=[]
mdl = IMP.Model()



topology='''
|molecule_name|color|fasta_fn|fasta_id|pdb_fn|chain|residue_range|pdb_offset|bead_size|em_residues_per_gaussian|rigid_body|super_rigid_body|chain_of_super_rigid_bodies|
|rpsG|orange|3ja1.fasta.txt|3JA1:SG|3ja1-pdb-bundle1.pdb|G|1,END| |10| |1|1| |
|rpsF|orange|3ja1.fasta.txt|3JA1:SF|3ja1-pdb-bundle1.pdb|V|1,END| |10| |1|1| |
|rpsE|orange|3ja1.fasta.txt|3JA1:SE|3ja1-pdb-bundle1.pdb|U|1,END| |10| |1|1| |
|rpsD|orange|3ja1.fasta.txt|3JA1:SD|3ja1-pdb-bundle1.pdb|T|1,END| |10| |1|1| |
|rpsC|orange|3ja1.fasta.txt|3JA1:SC|3ja1-pdb-bundle1.pdb|S|1,END| |10| |1|1| |
|rpsB|orange|3ja1.fasta.txt|3JA1:SB|3ja1-pdb-bundle1.pdb|R|1,END| |10| |1|1| |
|rplX|orange|3ja1.fasta.txt|3JA1:LW|3ja1-pdb-bundle2.pdb|E|1,END| |10| |1|1| |
|rplV|orange|3ja1.fasta.txt|3JA1:LU|3ja1-pdb-bundle2.pdb|C|1,END| |10| |1|1| |
|rpsN|orange|3ja1.fasta.txt|3JA1:SN|3ja1-pdb-bundle1.pdb|N|1,END| |10| |1|1| |
|rplT|orange|3ja1.fasta.txt|3JA1:LS|3ja1-pdb-bundle2.pdb|f|1,END| |10| |1|1| |
|rpsP|orange|3ja1.fasta.txt|3JA1:SP|3ja1-pdb-bundle1.pdb|P|1,END| |10| |1|1| |
|rplR|orange|3ja1.fasta.txt|3JA1:LQ|3ja1-pdb-bundle2.pdb|d|1,END| |10| |1|1| |
|rpsJ|orange|3ja1.fasta.txt|3JA1:SJ|3ja1-pdb-bundle1.pdb|J|1,END| |10| |1|1| |
|rpsI|orange|3ja1.fasta.txt|3JA1:SI|3ja1-pdb-bundle1.pdb|I|1,END| |10| |1|1| |
|rpsH|orange|3ja1.fasta.txt|3JA1:SH|3ja1-pdb-bundle1.pdb|H|1,END| |10| |1|1| |
|rplN|orange|3ja1.fasta.txt|3JA1:LM|3ja1-pdb-bundle2.pdb|c|1,END| |10| |1|1| |
|rplO|orange|3ja1.fasta.txt|3JA1:LN|3ja1-pdb-bundle2.pdb|W|1,END| |10| |1|1| |
|rpsU|orange|3ja1.fasta.txt|3JA1:SU|3ja1-pdb-bundle1.pdb|F|1,END| |10| |1|1| |
|rpsT|orange|3ja1.fasta.txt|3JA1:ST|3ja1-pdb-bundle1.pdb|E|1,END| |10| |1|1| |
|rplJ|orange|3ja1.fasta.txt|3JA1:LJ|3ja1-pdb-bundle2.pdb|V|1,END| |10| |1|1| |
|rplK|orange|3ja1.fasta.txt|3JA1:LK|3ja1-pdb-bundle2.pdb|X|1,END| |10| |1|1| |
|rpsQ|orange|3ja1.fasta.txt|3JA1:SQ|3ja1-pdb-bundle1.pdb|Q|1,END| |10| |1|1| |
|rplI|orange|3ja1.fasta.txt|3JA1:LI|3ja1-pdb-bundle2.pdb|Z|1,END| |10| |1|1| |
|rplF|orange|3ja1.fasta.txt|3JA1:LH|3ja1-pdb-bundle2.pdb|U|1,END| |10| |1|1| |
|rplD|orange|3ja1.fasta.txt|3JA1:LF|3ja1-pdb-bundle2.pdb|S|1,END| |10| |1|1| |
|rplE|orange|3ja1.fasta.txt|3JA1:LG|3ja1-pdb-bundle2.pdb|T|1,END| |10| |1|1| |
|rplQ|orange|3ja1.fasta.txt|3JA1:LP|3ja1-pdb-bundle2.pdb|b|1,END| |10| |1|1| |
|rplC|orange|3ja1.fasta.txt|3JA1:LE|3ja1-pdb-bundle2.pdb|M|1,END| |10| |1|1| |
|rplA|orange|3ja1.fasta.txt|3JA1:LC|3ja1-pdb-bundle2.pdb|L|1,END| |10| |1|1| |
|rplB|orange|3ja1.fasta.txt|3JA1:LD|3ja1-pdb-bundle2.pdb|B|1,END| |10| |1|1| |
|rpsO|orange|3ja1.fasta.txt|3JA1:SO|3ja1-pdb-bundle1.pdb|O|1,END| |10| |1|1| |
|rplW|orange|3ja1.fasta.txt|3JA1:LV|3ja1-pdb-bundle2.pdb|D|1,END| |10| |1|1| |
|rpsM|orange|3ja1.fasta.txt|3JA1:SM|3ja1-pdb-bundle1.pdb|M|1,END| |10| |1|1| |
|rpmE2|orange|3ja1.fasta.txt|3JA1:L2|3ja1-pdb-bundle2.pdb|K|1,END| |10| |1|1| |
|rplU|orange|3ja1.fasta.txt|3JA1:LT|3ja1-pdb-bundle2.pdb|g|1,END| |10| |1|1| |
|rpsK|orange|3ja1.fasta.txt|3JA1:SK|3ja1-pdb-bundle1.pdb|K|1,END| |10| |1|1| |
|rpsL|orange|3ja1.fasta.txt|3JA1:SL|3ja1-pdb-bundle1.pdb|L|1,END| |10| |1|1| |
|rplS|orange|3ja1.fasta.txt|3JA1:LR|3ja1-pdb-bundle2.pdb|e|1,END| |10| |1|1| |
|rpmI|orange|3ja1.fasta.txt|3JA1:L6|3ja1-pdb-bundle2.pdb|Q|1,END| |10| |1|1| |
|rpmH|orange|3ja1.fasta.txt|3JA1:L5|3ja1-pdb-bundle2.pdb|P|1,END| |10| |1|1| |
|rpmJ|orange|3ja1.fasta.txt|3JA1:L7|3ja1-pdb-bundle2.pdb|R|1,END| |10| |1|1| |
|rplP|orange|3ja1.fasta.txt|3JA1:LO|3ja1-pdb-bundle2.pdb|a|1,END| |10| |1|1| |
|rpmA|orange|3ja1.fasta.txt|3JA1:LY|3ja1-pdb-bundle2.pdb|G|1,END| |10| |1|1| |
|rpmC|orange|3ja1.fasta.txt|3JA1:L0|3ja1-pdb-bundle2.pdb|I|1,END| |10| |1|1| |
|rpmB|orange|3ja1.fasta.txt|3JA1:LZ|3ja1-pdb-bundle2.pdb|H|1,END| |10| |1|1| |
|rpmE|orange|3ja1.fasta.txt|3JA1:L2|3ja1-pdb-bundle2.pdb|K|1,END| |10| |1|1| |
|rpmD|orange|3ja1.fasta.txt|3JA1:L1|3ja1-pdb-bundle2.pdb|J|1,END| |10| |1|1| |
|rpmG|orange|3ja1.fasta.txt|3JA1:L4|3ja1-pdb-bundle2.pdb|O|1,END| |10| |1|1| |
|rpmF|orange|3ja1.fasta.txt|3JA1:L3|3ja1-pdb-bundle2.pdb|N|1,END| |10| |1|1| |
|rpmJ2|orange|3ja1.fasta.txt|3JA1:L7|3ja1-pdb-bundle2.pdb|R|1,END| |10| |1|1| |
|rplM|orange|3ja1.fasta.txt|3JA1:LL|3ja1-pdb-bundle2.pdb|Y|1,END| |10| |1|1| |
|fusA|orange|3ja1.fasta.txt|3JA1:S3|3ja1-pdb-bundle1.pdb|X|1,END| |10| |1|1| |
|rpsS|orange|3ja1.fasta.txt|3JA1:SS|3ja1-pdb-bundle1.pdb|A|1,END| |10| |1|1| |
|rpsR|orange|3ja1.fasta.txt|3JA1:SR|3ja1-pdb-bundle1.pdb|W|1,END| |10| |1|1| |
'''

# Normally the topology table is kept in a text file but here we just write it to a temporary one
tf = tempfile.NamedTemporaryFile(delete=False,mode='w+')
tf.write(topology)
tf.close()

# The TopologyReader reads the text file, and the BuildSystem macro constructs it

reader = IMP.pmi.topology.TopologyReader(tf.name,
                                         pdb_dir = '.',
                                         fasta_dir = '.',
                                         gmm_dir = 'gmm')
bs = IMP.pmi.macros.BuildSystem(mdl)
bs.add_state(reader) # note you can call this multiple times to create a multi-state system




hier, dof = bs.execute_macro(max_rb_trans=4.0, max_rb_rot=0.3, max_bead_trans=4.0, max_srb_trans=4.0,max_srb_rot=0.3)


# Fix all rigid bodies but not Rpb4 and Rpb7 (the stalk)
# First select and gather all particles to fix.
fixed_particles=[]
for prot in ["rpsG"]:
    fixed_particles+=IMP.atom.Selection(hier,molecule=prot,copy_index=0).get_selected_particles()
# Fix the Corresponding Rigid movers using dof
# The flexible beads will still be flexible (fixed_beads is empty)!
#fixed_beads,fixed_rbs=dof.disable_movers(fixed_particles,[IMP.core.RigidBodyMover,IMP.pmi.TransformMover])

#IMP.pmi.plotting.topology.draw_component_composition(bs)


# Connectivity keeps things connected along the backbone (ignores if inside same rigid body)
mols=IMP.pmi.tools.get_molecules(hier)
connected_pairs={}
for mol in mols:
    molname = mol.get_name() + str(IMP.atom.Copy(mol).get_copy_index())
    IMP.pmi.tools.display_bonds(mol)
    cr = IMP.pmi.restraints.stereochemistry.ConnectivityRestraint(mol,scale=1.0)
    cr.add_to_model()
    cr.set_label("connect_"+molname)
    output_objects.append(cr)
    connected_pairs[mol]=cr.get_particle_pairs()



evr = IMP.pmi.restraints.stereochemistry.ExcludedVolumeSphere(included_objects = hier,resolution=10.0)
evr.add_to_model()
evr.set_weight(1.0)
evr.set_label("evr_"+molname)
output_objects.append(evr)


"""
# Shuffle the rigid body configuration of only the molecules we are interested in (Rpb4 and Rpb7)
# but all flexible beads will also be shuffled.
IMP.pmi.tools.shuffle_configuration(hier,
                                    excluded_rigid_bodies=fixed_rbs,
                                    max_translation=50,
                                    verbose=True,
                                    cutoff=5.0,
                                    niterations=100)
"""

import IMP.pmi.io
import IMP.pmi.io.crosslink
import IMP.pmi.restraints
import IMP.pmi.restraints.crosslinking
from IMP.pmi.io.crosslink import FilterOperator as FO
#import IMP.crosslinkms
#import IMP.crosslinkms.restraint_pmi2
import operator

rplp=IMP.pmi.io.crosslink.ResiduePairListParser("MSSTUDIO")
cldbkc=IMP.pmi.io.crosslink.CrossLinkDataBaseKeywordsConverter(rplp)
cldbkc.set_protein1_key("Protein 1")
cldbkc.set_protein2_key("Protein 2")
cldbkc.set_site_pairs_key("Selected Sites")
cldbkc.set_id_score_key("Score")
cldb=IMP.pmi.io.crosslink.CrossLinkDataBase(cldbkc)
cldb.set_name("master")

for csv in ["Resultat.b.csv"]:
    cldb_tmp=IMP.pmi.io.crosslink.CrossLinkDataBase(cldbkc)
    cldb_tmp.create_set_from_file(csv)
    cldb_tmp.set_name(csv)
    cldb.append_database(cldb_tmp)

renamedict={}
for xl in cldb:
     try:
         protname=xl[cldb.protein1_key].split("|")[2]
         renamedict[xl[cldb.protein1_key]]=protname
     except:
         pass
     try:
         protname=xl[cldb.protein2_key].split("|")[2]
         renamedict[xl[cldb.protein2_key]]=protname
     except:
         pass

cldb.rename_proteins(renamedict)


for xl in cldb:
    if xl["Type"]=="Self Linked":
       protname=xl[cldb.protein1_key]
       xl[cldb.protein2_key]=protname

print("Here")

cldb.align_sequence("alignment.txt")

rex=IMP.pmi.macros.ReplicaExchange0(mdl,
                                    root_hier=hier,                          # pass the root hierarchy
                                    monte_carlo_sample_objects=dof.get_movers(),  # pass MC movers
                                    global_output_directory="./A/",
                                    #crosslink_restraints=xls,
                                    output_objects=None,
                                    rmf_output_objects=output_objects,
                                    monte_carlo_steps=10,
                                    replica_exchange_minimum_temperature=1.0,
                                    replica_exchange_maximum_temperature=2.5,
                                    replica_exchange_swap=True,
                                    save_coordinates_mode="50th_score",
                                    number_of_best_scoring_models=0,      # set >0 to store best PDB files (but this is slow to do online)
                                    number_of_frames=1                   # increase number of frames to get better results!
                                    )
rex.execute_macro()

mcldb=IMP.pmi.io.crosslink.MapCrossLinkDataBaseOnStructure(cldb,IMP.pmi.output.RMFHierarchyHandler(mdl,"A/rmfs/0.rmf3"))
mcldb.compute_distances()
#mcldb.boxplot_crosslink_distances("Boxplot.pdf")

for xl in cldb:
    if xl[cldb.protein1_key] != xl[cldb.protein2_key]: 
       print(xl[cldb.protein1_key],xl[cldb.protein2_key],xl["Distance"])

cldb.save_csv("distance.csv")

cldbfs=IMP.pmi.io.crosslink.CrossLinkDataBaseFromStructure(system=bs.system)
print(cldbfs.get_all_possible_pairs())






